### Solutions to Courera's Machine Learning in MATLAB/Octave and Python

_Currently ongoing_

The [Machine Learning](https://www.coursera.org/learn/machine-learning/) course 
offered by Coursera is probably the de facto standard for an introduction to 
machine learning techniques. But Andrew Ng decided to teach the
course using MATLAB/Octave. For a two-fold learning experience, I've
reimplemented all code in Python.

Here are my solutions to the assignments, and then again in Python. 
For Python I've tried to try as true to source as possible (for better or worse).
