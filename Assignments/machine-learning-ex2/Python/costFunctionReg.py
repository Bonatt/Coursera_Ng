import numpy as np
from sigmoid import *

def costFunctionReg(theta, X, y, Lambda):
    m = len(y)
    y = y.values.reshape(m,1) # (100,) --> (100,1)
    h = sigmoid(np.dot(X,theta))
    J = 1/m * (np.dot(-y.T, np.log(h)) - np.dot((1-y).T, np.log(1-h))) \
        + Lambda/(2*m) * np.dot(theta[1:].T,theta[1:])
    #gradJ = 1/m * (np.dot(X.T,h) - np.dot(X.T,y))
    #gradJ[1:] = gradJ[1:] + Lambda/m*theta[1:]
    gradJ = 1/m * (np.dot(X.T,h) - np.dot(X.T,y)) + Lambda/m*theta
    gradJ[0] = gradJ[0] - Lambda/m*theta[0]
    return J, gradJ


### These next two functions are for minizing... 
# Not working unless separate. Not sure why. By design?
def costFunctionReg2(theta, X, y, Lambda):
    m = len(y)
    y = y.values.reshape(m,1) # (100,) --> (100,1)
    h = sigmoid(np.dot(X,theta))
    J = 1/m * (np.dot(-y.T, np.log(h)) - np.dot((1-y).T, np.log(1-h))) \
        + Lambda/(2*m) * np.dot(theta[1:].T,theta[1:])
    #print('m \t y \t h \t theta \t J')
    #print(m, y.shape, h.shape, theta.shape, J.shape)
    return J

def costFunctionReg3(theta, X, y, Lambda):
    m = len(y)
    y = y.values.reshape(m,1) # (100,) --> (100,1)
    h = sigmoid(np.dot(X,theta))
    gradJ = 1/m * (np.dot(X.T,h) - np.dot(X.T,y)) + Lambda/m*theta
    #print('m \t y \t h \t theta \t gradJ')
    #print(m, y.shape, h.shape, theta.shape, gradJ.shape)
    gradJ[0] = gradJ[0] - Lambda/m*theta[0]
    return gradJ
