from sigmoid import *

def predict(theta, X):
    h = sigmoid( np.dot(X,theta) )
    p = h >= 0.5
    return p
