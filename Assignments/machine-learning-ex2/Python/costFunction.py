import numpy as np
from sigmoid import *

def costFunction(theta, X, y):
    m = len(y)
    y = y.values.reshape(m,1) # (100,) --> (100,1)
    h = sigmoid(np.dot(X,theta))
    J = 1/m * np.sum( -y*np.log(h) - (1-y)*np.log(1-h))
    gradJ = 1/m * (np.dot(X.T,h) - np.dot(X.T,y))
    return J, gradJ

def costFunction2(params):
    theta, X, y = params
    m = len(y)
    y = y.values.reshape(m,1) # (100,) --> (100,1)
    h = sigmoid(np.dot(X,theta))
    J = 1/m * np.sum( -y*np.log(h) - (1-y)*np.log(1-h))
    gradJ = 1/m * (np.dot(X.T,h) - np.dot(X.T,y))
    return J, gradJ


### These next two functions are for minizing... 
# Not working unless separate. Not sure why. By design?
def costFunction3(theta, X, y):
    m = len(y)
    #y = y.values.reshape(m,1) # (100,) --> (100,1)
    h = sigmoid(np.dot(X,theta))
    #J = 1/m * np.sum( -y*np.log(h) - (1-y)*np.log(1-h))
    J = (1/m) * (np.dot(-y.T, np.log(h)) - np.dot((1-y).T, np.log(1-h)))
    #gradJ = 1/m * (np.dot(X.T,h) - np.dot(X.T,y))
    #print(type(J),type(gradJ))
    #return float(J), list(gradJ)
    return J

def costFunction4(theta,X,y):
    m = len(y)
    #y = y.values.reshape(m,1)
    h = sigmoid(np.dot(X,theta))
    #gradJ = 1/m * (np.dot(X.T,h) - np.dot(X.T,y))
    gradJ = 1/m * np.dot(X.T,h-y)
    return gradJ


'''
h = sigmoid(X*theta);
J = 1/m * sum( -y.*log(h) - (1-y).*log(1-h) );
grad = 1/m * (X'*h - X'*y);
'''
