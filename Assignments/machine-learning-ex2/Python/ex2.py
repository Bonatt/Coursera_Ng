### See original PDF and MATLAB code for documention unless otherwise stated here.
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
#from scipy.optimize import fmin_bfgs #minimize
from scipy import optimize
#from mpl_toolkits.mplot3d import Axes3D
a = 'dark_background'
b = 'seaborn-muted' # Default?
c = 'fivethirtyeight'
d = 'ggplot'
plt.style.use(b)
colormap = cm.viridis
print()




### Load Data
# The first two columns contains the exam scores and 
#  the third column contains the label.

data = pd.read_csv('ex2data1.txt', header=None, names=['exam1', 'exam2', 'label'])
X = data.iloc[:,:-1] # Take all but last column (I guess -1 is not incluive here)
y = data.iloc[:,-1] # Take last column
m = len(y)

# ==================== Part 1: Plotting ====================
#  We start the exercise by first plotting the data to understand the
#  the problem we are working with.
print('Plotting data with "+" indicating (y = 1) examples',
      ' and "o" indicating (y = 0) examples.')
#plt.figure()
#axes = data.columns.values[:-1] #list(data)[:-1]
#axes = axes+' scores'
axes = ['Exam 1 scores', 'Exam 2 scores']
labels = ['Amitted', 'Not admitted']
from plotData import *
plotData(X,y, axes, labels)




# ============ Part 2: Compute Cost and Gradient ============
#  In this part of the exercise, you will implement the cost and gradient
#  for logistic regression. You neeed to complete the code in costFunction.m

###  Setup the data matrix appropriately, and add ones for the intercept term
m, n = X.shape
# Add intercept term to x and X_test
X.insert(0, 'ones', np.ones(m))

# Initialize fitting parameters
initial_theta = np.zeros((n+1, 1))

# Compute and display initial cost and gradient
from costFunction import *
# Regaring the necessity of params, see
# https://stackoverflow.com/questions/13670333/multiple-variables-in-scipys-optimize-minimize
# Is necessary for minimize function later.
params = [initial_theta, X, y]
cost, grad = costFunction2(params)
print('Cost at initial theta (zeros):', cost)
print('Expected cost (approx): 0.693')
print('Gradient at initial theta (zeros):\n', grad)
print('Expected gradients (approx):\n -0.1000\n -12.0092\n -11.2628\n')

# Compute and display cost and gradient with non-zero theta
test_theta = [[-24], [0.2], [0.2]]
params = [test_theta, X, y]
cost, grad = costFunction2(params);
print('Cost at test theta:', cost)
print('Expected cost (approx): 0.218')
print('Gradient at test theta:\n', grad)
print('Expected gradients (approx):\n 0.043\n 2.566\n 2.647\n')




# ============= Part 3: Optimizing using fminunc  =============
#  In this exercise, you will use a built-in function (fminunc) to find the
#  optimal parameters theta.
# SciPy equivalent is fmin_bfgs
# https://stackoverflow.com/questions/18801002/fminunc-alternate-in-numpy

#options = {'maxiter': 400}
#test = optimize.minimize(costFunction, x0=initial_theta, args=(X,y), method='bfgs')
#params = [list(initial_theta), list(X.values), list(y.values)]
#test = optimize.minimize(costFunction2, x0=params, method='bfgs', options=options)
#X = X.values # DF --> ndarry
#y = y.values.reshape((m,1)) # ", (m,) --> (m,1)

Result = optimize.minimize(costFunction3, x0=initial_theta, args=(X,y), method='bfgs', jac=costFunction4)
#Result = optimize.minimize(costFunction, x0=initial_theta, args=(X,y), method='bfgs', jac=True)
#Result = optimize.minimize(costFunction, x0=initial_theta, args=(X,y), method='bfgs', jac=False)
theta = Result.x.reshape(len(initial_theta),1)

cost, grad = costFunction(theta, X, y)
# Print theta to screen
print('Cost at theta found by minimize.bgfs:', cost)
print('Expected cost (approx): 0.203')
print('theta:\n', theta)
print('Expected theta (approx):\n-25.161\n 0.206\n 0.201\n')

#% Plot Boundary
from plotDecisionBoundary import *
plotDecisionBoundary(theta, X, y, axes, labels+['Decision boundary'], new=False) # new == new plot?



# ============== Part 4: Predict and Accuracies ==============
#  After learning the parameters, you'll like to use it to predict the outcomes
#  on unseen data. In this part, you will use the logistic regression model
#  to predict the probability that a student with score 45 on exam 1 and
#  score 85 on exam 2 will be admitted.
#
#  Furthermore, you will compute the training and test set accuracies of
#  our model.
#
#  Your task is to complete the code in predict.m
#
#  Predict probability for a student with score 45 on exam 1
#  and score 85 on exam 2
ex1 = 45
ex2 = 85
prob = sigmoid( np.dot([1, ex1, ex2], theta) )
print('For a student with scores 45 and 85, we predict an admission', 
         'probability of', prob)
print('Expected value: 0.775 +/- 0.002')

# Compute accuracy on our training set
from predict import *
p = predict(theta, X)

print('Train Accuracy:', np.mean(p == y.reshape(m,1)) * 100, '%')
print('Expected accuracy (approx): 89.0 %')
