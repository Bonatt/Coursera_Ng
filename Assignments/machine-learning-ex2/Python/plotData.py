import matplotlib.pyplot as plt

def plotData(X, y, axes, labels):

    # Find all 1,0 values in y, assign boolean arrays to pos,neg.
    pos = y == 1
    neg = y == 0

    # Since there should only be two features/columns to plot, don't loop.
    #  e.g., for column in X: 
    
    admitted_exam1 = X[pos].iloc[:,0] # == X[pos][label[0]] == X[pos].exam1
    admitted_exam2 = X[pos].iloc[:,1]
    nadmitted_exam1 = X[neg].iloc[:,0]
    nadmitted_exam2 = X[neg].iloc[:,1]

    plt.figure()
    plt.plot( admitted_exam1, admitted_exam2, 'k+', label=labels[0])
    plt.plot( nadmitted_exam1, nadmitted_exam2, 'k.', label=labels[1])
    plt.ylabel(axes[0])
    plt.xlabel(axes[1])
    plt.legend()
    plt.show(block=False)
