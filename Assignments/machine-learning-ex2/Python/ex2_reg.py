### See original PDF and MATLAB code for documention unless otherwise stated here.
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
#from scipy.optimize import fmin_bfgs #minimize
from scipy import optimize
#from mpl_toolkits.mplot3d import Axes3D
a = 'dark_background'
b = 'seaborn-muted' # Default?
c = 'fivethirtyeight'
d = 'ggplot'
plt.style.use(b)
colormap = cm.viridis
print()




### Load Data
# The first two columns contains the X values and the third column
#  contains the label (y).
data = pd.read_csv('ex2data2.txt', header=None, names=['test1', 'test2', 'label'])
X = data.iloc[:,:-1] # Take all but last column (I guess -1 is not incluive here)
y = data.iloc[:,-1] # Take last column
m = len(y)

axes = ['Microchip test 1', 'Microchip test 2']
labels = ['Accepted', 'Rejected'] #['y = 1', 'y = 0']
from plotData import *
plotData(X,y, axes, labels)




# =========== Part 1: Regularized Logistic Regression ============
#  In this part, you are given a dataset with data points that are not
#  linearly separable. However, you would still like to use logistic
#  regression to classify the data points.
#
#  To do so, you introduce more features to use -- in particular, you add
#  polynomial features to our data matrix (similar to polynomial
#  regression).

# Add Polynomial Features

# Note that mapFeature also adds a column of ones for us, so the intercept
# term is handled
from mapFeature import *
X = mapFeature( X.iloc[:,0], X.iloc[:,1], degree=6)
X = np.asarray(X).T # No this line is mapFeature outputs DF
m, n = X.shape

# Initialize fitting parameters
initial_theta = np.zeros( (n,1) )
# Set regularization parameters lambda to 1
Lambda = 1

# Compute and display initial cost and gradient for 
# regularized logistic regression
from costFunctionReg import *
cost, grad = costFunctionReg(initial_theta, X, y, Lambda)

print('Cost at initial theta (zeros):', cost)
print('Expected cost (approx): 0.693')
print('Gradient at initial theta (zeros) - first five values only:\n', grad[:5])
print('Expected gradients (approx) - first five values only:')
print(' 0.0085\n 0.0188\n 0.0001\n 0.0503\n 0.0115\n')

test_theta = np.ones( (n,1) )
Lambda = 10
cost, grad = costFunctionReg(test_theta, X, y, Lambda)
print('Cost at test theta (ones) (with lambda = 10):', cost)
print('Expected cost (approx): 3.16')
print('Gradient at test theta - first five values only:\n', grad[:5])
print('Expected gradients (approx) - first five values only:')
print(' 0.3460\n 0.1614\n 0.1948\n 0.2269\n 0.0922\n')




# ============= Part 2: Regularization and Accuracies =============
#  Optional Exercise:
#  In this part, you will get to try different values of lambda and
#  see how regularization affects the decision coundart
#
#  Try the following values of lambda (0, 1, 10, 100).
#
#  How does the decision boundary change when you vary lambda? How does
#  the training set accuracy vary?


# Initialize fitting parameters
initial_theta = np.zeros( (n,1) )
# Set regularization parameter lambda to 1 (you should vary this)
Lambda = 1

# Optimize. Faults (array dim mismatch) if jac=on.
Result = optimize.minimize(costFunctionReg2, x0=initial_theta, 
                           args=(X,y,Lambda), method='bfgs')#, 
                           #jac=costFunctionReg3)
theta = Result.x.reshape(initial_theta.shape)

# Plot boundary
from plotDecisionBoundary import *
title = 'Lambda = '+str(Lambda)
plotDecisionBoundary(theta, X, y, axes+[title], labels+['Decision boundary'], new=False, degree=6)


# Compute accuracy n our training set
from predict import *
p = predict(theta, X)

print('Train Accuracy:', np.mean(p == y.reshape(m,1)) * 100, '%')
print('Expected accuracy (with lambda = 1) (approx): 83.1 %')

