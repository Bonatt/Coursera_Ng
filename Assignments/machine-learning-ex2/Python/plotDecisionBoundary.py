import matplotlib.pyplot as plt
import numpy as np
from plotData import *
from mapFeature import *

def plotDecisionBoundary(theta, X, y, axes, labels, new=False, **kwargs):

    #print(kwargs)

    if X.shape[1] <= 3:
        # Only need 2 points to define a line, so choose two endpoints
        plot_x = [X.min(axis=0)[1], X.max(axis=0)[1]]
        # [min(X.iloc[:,1]), max(X.iloc[:,1])]
        plot_y = -1/theta[2] * (theta[1] * plot_x + theta[0])

       # Plot this line on previous plot
        if new == False:
            plt.plot(plot_x, plot_y, label=labels[-1])
            plt.legend()
            plt.show(block=False)
        # Plot data AND this line on new plot
        if new == True:
            # "Remove" added ones colum in zeroth position
            #X = X.iloc[:,1:] # If X is a DF, otherwise:
            X = X.T[1:]
            #plt.figure()
            plotData(X,y,axes,labels)
            plt.plot(plot_x, plot_y, label=labels[-1])
            plt.legend()
            plt.show(block=False)

    else:
        plot_x = [X.min(axis=0)[1], X.max(axis=0)[1]]
        plot_y = -1/theta[2] * (theta[1] * plot_x + theta[0])
        
        # Grid range
        u = np.linspace(-1, 1.5, 50)
        v = np.linspace(-1, 1.5, 50)
        z = np.zeros( (len(u),len(v)) )

        # Evaluate z = theta*x over grid
        for i in range(len(u)):
            for j in range(len(v)):
                uv = mapFeature( np.array(u[i]), np.array(v[j]), \
                                 degree=kwargs['degree'] )
                #print(uv)
                #print(type(uv), type(theta))
                #print(uv.shape, theta.shape)
                #print(np.shape(uv), np.shape(theta))
                #z[i,j] = np.dot(uv.values, theta)
                z[i,j] = np.dot(uv, theta)
        #z = z.T
        #plt.contour(u, v, z)
        

       # Plot this line on previous plot
        if new == False:
            C = plt.contour(u, v, z, levels=[0])# label=labels[-1])
            C.collections[0].set_label(labels[-1])
            plt.legend()
            plt.title(axes[-1])
            plt.show(block=False)
        '''
        # Plot data AND this line on new plot
        if new == True:
            # "Remove" added ones colum in zeroth position
            #X = X.iloc[:,1:] # If X is a DF, otherwise:
            X = X.T[1:]
            #plt.figure()
            plotData(X,y,axes,labels)
            plt.plt.contour(u, v, z, levels=[0])
            plt.legend()
            plt.show(block=False)
        '''

'''
u = np.linspace(-1, 1.5, 50)
v = np.linspace(-1, 1.5, 50)
z = np.zeros( (len(u),len(v)) )
for i in range(len(u)):
  for j in range(len(v)):
    uv = mapFeature( np.array(u[i]), np.array(v[j]), degree=6)
    print(uv)
    print(type(uv), type(theta))
    print(uv.shape, theta.shape)
    z[i,j] = np.dot(uv, theta)
'''

















