import pandas as pd

# MAPFEATURE Feature mapping function to polynomial features
#
#   MAPFEATURE(X1, X2) maps the two input features
#   to quadratic features used in the regularization exercise.
#
#   Returns a new feature array with more features, comprising of
#   X1, X2, X1.^2, X2.^2, X1*X2, X1*X2.^2, etc..
#
#   Inputs X1, X2 must be the same size
'''
def mapFeature(X1, X2, degree):

    # Assuming type(X) == DF
    out = pd.DataFrame()
   
    col = 0 
    for i in range(degree+1):
        for j in range(i+1):
            out[col] = X1**(i-j) * X2**j
            print(out[col])
            col += 1

    return out
'''

def mapFeature(X1, X2, degree):

    out = []
    for i in range(degree+1):
        for j in range(i+1):
            out.append( X1**(i-j) * X2**j )

    return out
