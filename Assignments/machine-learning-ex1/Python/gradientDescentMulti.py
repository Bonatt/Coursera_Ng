import numpy as np
from computeCostMulti import *

def gradientDescentMulti(X, y, theta, alpha, iters):
    m = len(y)
    ncols = X.shape[1]
    thetai = np.zeros((ncols,1))
    J_hist = []

    for iters in range(iters):
        h = np.dot(X, theta)
        
        for i in range(ncols):
            thetai[i] = theta[i] - alpha/m * np.sum( (h.T - y.values)*X.iloc[:,i].values)
        
        theta = thetai
        
        J = computeCostMulti(X, y, theta)
        J_hist.append(J)

    return theta, J_hist
