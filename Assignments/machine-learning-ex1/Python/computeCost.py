import numpy as np

def computeCost(X, y, theta):
    m = len(y)
    h = np.dot(X, theta)
    J = 1/(2.*m) * np.sum( (h.T - y.values)**2 )
    return J
