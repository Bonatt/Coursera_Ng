import matplotlib.pyplot as plt

def plotData(x, y, label):
    plt.plot(x,y, 'rx', markersize=10, label=label)
    plt.ylabel('Profit in $10,000s')
    plt.xlabel('Population of City in 10,000s')
    plt.show(block=False)
