import numpy as np
from computeCost import *

def gradientDescent(X, y, theta, alpha, iters):
    m = len(y)

    theta_hist = [ [[0],[0]] ]
    J_hist = []

    for i in range(iters):
        h = np.dot(X, theta)
        #print('h=',h[:5])

        theta0 = float(theta[0] - alpha/m * np.sum( (h.T - y.values)*X.iloc[:,0].values ))
        theta1 = float(theta[1] - alpha/m * np.sum( (h.T - y.values)*X.iloc[:,1].values ))

        theta = [ [theta0], [theta1] ]    
        #print('theta=',theta)        
        theta_hist.append(theta)

        J = computeCost(X, y, theta)
        #print('J=',J)
        J_hist.append(J)

    return theta, theta_hist, J_hist
