### See original PDF and MATLAB code for documention unless otherwise stated here.
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
a = 'dark_background'
b = 'seaborn-muted' # Default?
c = 'fivethirtyeight'
d = 'ggplot'
plt.style.use(b)
colormap = cm.viridis 
print()



# ==================== Part 1: Basic Function ====================
print('\n Running warmUpExercise ...')
print('5x5 Identitiy Matrix:')
from warmUpExercise import *
warmUpExercise()

#print('Program paused. Press enter to continue')
#input()




# ==================== Part 2: Plotting ====================
print('\n Plotting Data ...')
data = pd.read_csv('ex1data1.txt', header=None, names=['popu', 'profit'])
X = data.iloc[:,0] #data.['popu']
y = data.iloc[:,1] #data.['profit']
m = len(y) # Number of training examples

# Plot data
plt.figure()
from plotData import *
plotData(X, y, 'Training data')




# ==================== Part 3: Cost and Gradient descent ====================
# Append [1,1,...] to DataFrame data, then make new X variable as [ones, 'popu']
#data['ones'] = np.ones(m)
#X = data[['ones', 'popu']]
data.insert(0, 'ones', np.ones(m))
X2 = data.iloc[:,:2]

# Compute and display initial cost
print('\n Testing the cost function J(theta) ...')
theta = np.zeros((2,1))
from computeCost import *
J = computeCost(X2, y, theta)
print('With hypothesis theta=(0,0), computed cost value =', J)
print('  Expected cost value (approx) = 32.07')

# Further testing of the cost function
theta = np.array([[-1.],[2.]]) 
J = computeCost(X2, y, theta)
print('With theta=(-1,2), computed cost value =', J)
print('  Expected cost value (approx) = 54.24')


# Run gradient descent
print('\n Running batch gradient descent ...')
theta = np.zeros((2,1))
alpha = 0.01
iterations = 1500
from gradientDescent import *
theta, theta_hist, J_hist = gradientDescent(X2, y, theta, alpha, iterations)
print('Found theta =', theta)
print('  Expected theta (approx) = (-3.6303, 1.1664)')

# h = hypothesis...
h = np.dot(X2, theta)

# Plot the linear fit
plt.plot(X, h, '-', label='Linear regression')
plt.legend()
plt.show(block=False)

# Predict values for population sizes of 35,000 and 70,000
predict1 = np.dot([1, 3.5], theta)
print('For population = 35,000, we predict a profit of', predict1*10000)
predict2 = np.dot([1, 7], theta)
print('For population = 70,000, we predict a profit of', predict2*10000)




# ============= Part 4: Visualizing J(theta_0, theta_1) =============
print('\n Visualizing J(theta_0, theta_1) ...')

# Grid over which we will calculate J
theta0_vals = np.linspace(-10, 10, 100) #np.arange(-10,10.2, 0.2) # 1D
theta1_vals = np.linspace(-1, 4, 100) #np.arange(-1,4.05, 0.05) # 1D
J_vals = np.array([[computeCost(X2,y,np.array([[t0],[t1]])) for t0 in theta0_vals] for t1 in theta1_vals]) # 2D

# 3D plot of cost function. For whatever reason, plotting 3D plots is hard (for me (to understand?)?)
# https://stackoverflow.com/questions/9170838/surface-plots-in-matplotlib
xx, yy = np.meshgrid(theta0_vals, theta1_vals)
zz = J_vals#.T

fig3d = plt.figure()
ax = fig3d.gca(projection='3d')
surf = ax.plot_surface(xx,yy,zz, cmap=colormap, linewidth=0, antialiased=False)
plt.xlabel(r'$\theta_0$')
plt.ylabel(r'$\theta_1$')
ax.set_zlabel(r'J($\theta$)')
plt.show(block=False)

# Contour plot of cost function.
plt.figure()
contour = plt.contour(xx, yy, zz, 10, cmap=colormap)#, colors='k') np.logspace(-2, 3, 20)
plt.clabel(contour, inline=1)
plt.plot(theta[0], theta[1], 'rx')

# Plot trail of J(theta)
theta0_hist = [t[0][0] for t in theta_hist]
theta1_hist = [t[1][0] for t in theta_hist]
plt.plot(theta0_hist, theta1_hist, 'r-')

plt.xlabel(r'$\theta_0$')
plt.ylabel(r'$\theta_1$')
plt.show(block=False)



### Beyond the call of duty. Plot J wrt to iteration
plt.figure()
plt.plot(range(len(J_hist)), J_hist, '-')
plt.xlabel(r'Iterations with $\alpha$ of '+str(alpha))
plt.ylabel(r'J($\theta$)')
plt.show(block=False)
