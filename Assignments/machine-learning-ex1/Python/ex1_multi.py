### See original PDF and MATLAB code for documention unless otherwise stated here.
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
a = 'dark_background'
b = 'seaborn-muted' # Default?
c = 'fivethirtyeight'
d = 'ggplot'
plt.style.use(b)
colormap = cm.viridis
print()




# ================ Part 1: Feature Normalization ================
print('Loading data ...')

data = pd.read_csv('ex1data2.txt', header=None)
X = data.iloc[:,:-1] # Take all but last column (I guess -1 is not incluive here)
y = data.iloc[:,-1] # Take last column
m = len(y)

print('First 10 examples from the dataset:')
print(data.head())


print('Normalizing features ...')
from featureNormalize import *
X_norm, mu, sigma = featureNormalize(X)

# Add intercept term to X
X_norm.insert(0, 'ones', np.ones(m))




# ================ Part 2: Gradient Descent ================
print('Running gradient descent ...')
alpha = 0.01
num_iters = 400

# Init theta and run gradient descent
ncols = X_norm.shape[1]
theta = np.zeros((ncols,1))
from gradientDescentMulti import *
theta_gd, J_history = gradientDescentMulti(X_norm, y, theta, alpha, num_iters)

plt.figure()
plt.plot(range(len(J_history)), J_history, '-b')
plt.xlabel('Number of iterations')
plt.ylabel('Cost J')
plt.show(block=False)

print('Theta computed from gradient descent:')
print(theta_gd)

# Estimate the price of a 1650 sq-ft, 3 br house
# Recall that the first column of X is all-ones. Thus, it does
# not need to be normalized.
# This whole grouping here, just below, is a total mess...
sqft = 1650
beds = 3
'''
price_gd = ((sqft,beds)-mu)/sigma
price_gd = list(price_gd.values)
price_gd.insert(0,1)
price_gd = price_gdi.T * theta_gd
'''
#price_gd = ((1,sqft,beds)-np.insert(mu.values,0,1))/np.insert(sigma.values, 0, 0)
price_gd = np.dot( np.insert((((sqft,beds)-mu)/sigma).values, 0, 1), theta_gd)

print('Predicted price of a 1650 sq-ft, 3 br house', 
      '(using gradient descent):', price_gd)




# ================ Part 3: Normal Equations ================
X.insert(0, 'ones', np.ones(m))

print('Solving with normal equation ...')

from normalEqn import *
theta_ne = normalEqn(X,y)

print('Theta computed from the normal equation:')
print(theta_ne)

price_ne = np.dot( (1,sqft,beds), theta_ne )

print('Predicted price of a 1650 sq-ft 3 br house',
      '(using normal equation):', price_ne)

