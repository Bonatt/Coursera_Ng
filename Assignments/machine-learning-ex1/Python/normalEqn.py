import numpy as np

def normalEqn(X,y):
  # Wrong:
  #theta = np.linalg.inv(X.T*X) * X.T * y
  # Right 1, 2:
  #theta = np.dot( np.dot( np.linalg.inv(np.dot(X.T, X)), X.T ), y )
  theta = np.linalg.inv(X.T.dot(X)).dot(X.T).dot(y)
  return theta
