import numpy as np

def computeCostMulti(X, y, theta):
    m = len(y)
    h = np.dot(X, theta)
    J = 1/(2.*m) * np.dot( h.T-y.values, (h.T-y.values).T )
    return float(J)
